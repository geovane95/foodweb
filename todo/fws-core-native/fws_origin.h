/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#ifndef FWS_ORIGIN
#define FWS_ORIGIN

#include "../fws-core-truth/fws_crucial_h.h"

//#struct fws_ds_origin
typedef struct fws_ds_origin
{
	void *fws_content;
} FWS_DS_ORIGIN;
//#struct fws_ds_origin

//#struct fws_ds_data_decimal
typedef struct fws_ds_data_decimal
{
	long fws_value;
} FWS_DS_DATA_DECIMAL;
//#struct fws_ds_data_decimal

//#struct fws_ds_data_bool
typedef struct fws_ds_data_bool
{
	char fws_value;
} FWS_DS_DATA_BOOL;
//#struct fws_ds_data_bool

//#struct fws_ds_data_existence
typedef struct fws_ds_data_existence
{
	void *fws_to_existence;
} FWS_DS_DATA_EXISTENCE;
//#struct fws_ds_data_existence

//#FWS_DS_ORIGIN *fws_return_data_decimal
const FWS_DS_ORIGIN *fws_return_data_decimal(FWS_DS_DATA_DECIMAL);
//#FWS_DS_ORIGIN *fws_return_data_decimal

//#FWS_DS_ORIGIN *fws_return_data_bool
const FWS_DS_ORIGIN *fws_return_data_bool(FWS_DS_DATA_BOOL);
//#FWS_DS_ORIGIN *fws_return_data_bool

//#FWS_DS_ORIGIN *fws_return_data_existence
const FWS_DS_ORIGIN *fws_return_data_existence(FWS_DS_DATA_EXISTENCE);
//#FWS_DS_ORIGIN *fws_return_data_existence

#endif
