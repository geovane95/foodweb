/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "fws_origin.h"

//#FWS_DS_ORIGIN *fws_return_data_decimal
const FWS_DS_ORIGIN *fws_return_data_decimal(FWS_DS_DATA_DECIMAL fws_decimal_ref)
{
	FWS_DS_ORIGIN *fws_grep_val = NULL;
	if( ( fws_grep_val = (FWS_DS_ORIGIN *) malloc(sizeof(FWS_DS_ORIGIN)) )== NULL || fws_decimal_ref == NULL)
		return NULL;

	fws_grep_val->fws_content = &fws_decimal_ref.fws_value;
	return (const FWS_DS_ORIGIN *) fws_grep_val;
}
//#FWS_DS_ORIGIN *fws_return_data_decimal

//#FWS_DS_ORIGIN *fws_return_data_bool
const FWS_DS_ORIGIN *fws_return_data_bool(FWS_DS_DATA_BOOL fws_bool_ref)
{
	FWS_DS_ORIGIN *fws_grep_val = NULL;
	if( ( fws_grep_val = (FWS_DS_ORIGIN *) malloc(sizeof(FWS_DS_ORIGIN)) )== NULL || fws_bool_ref == NULL)
		return NULL;

	fws_grep_val->fws_content = &fws_bool_ref.fws_value;
	return (const FWS_DS_ORIGIN *) fws_grep_val;
}
//#FWS_DS_ORIGIN *fws_return_data_bool

//#FWS_DS_ORIGIN *fws_return_data_existence
const FWS_DS_ORIGIN *fws_return_data_existence(FWS_DS_DATA_EXISTENCE fws_existence_ref)
{
	FWS_DS_ORIGIN *fws_grep_val = NULL;
	if( ( fws_grep_val = (FWS_DS_ORIGIN *) malloc(sizeof(FWS_DS_ORIGIN)) )== NULL || fws_existence_ref == NULL)
		return NULL;

	fws_grep_val->fws_content = fws_existence_ref.fws_to_existence;
	return (const FWS_DS_ORIGIN *) fws_grep_val;
}
//#FWS_DS_ORIGIN *fws_return_data_existence
