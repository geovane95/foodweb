/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "fws_outward.h"

//#FWS_DS_OUTWARD *fws_return_visibility_enviorment
const FWS_DS_OUTWARD *fws_return_visibility_enviorment(void)
{
	FWS_DS_OUTWARD *fws_global_visibility = NULL;
	if( (fws_global_visibility = (FWS_DS_OUTWARD *) malloc(sizeof(FWS_DS_OUTWARD)) )== NULL )
		return NULL;

	fws_global_visibility->fws_justify = FWS_WATCH_LIKE_ENVIORMENT;
	return (const FWS_DS_OUTWARD *) fws_global_visibility;
}
//#FWS_DS_OUTWARD *fws_return_visibility_enviorment

//#FWS_DS_OUTWARD *fws_return_visibility_confidential
const FWS_DS_OUTWARD *fws_return_visibility_confidential(void)
{
	FWS_DS_OUTWARD *fws_global_visibility = NULL;
	if( (fws_global_visibility = (FWS_DS_OUTWARD *) malloc(sizeof(FWS_DS_OUTWARD)) )== NULL )
		return NULL;

	fws_global_visibility->fws_justify = FWS_WATCH_LIKE_CONFIDENTIAL;
	return (const FWS_DS_OUTWARD *) fws_global_visibility;
}
//#FWS_DS_OUTWARD *fws_return_visibility_confidential

//#FWS_DS_OUTWARD *fws_return_visibility_blocked
const FWS_DS_OUTWARD *fws_return_visibility_blocked(void)
{
	FWS_DS_OUTWARD *fws_global_visibility = NULL;
	if( (fws_global_visibility = (FWS_DS_OUTWARD *) malloc(sizeof(FWS_DS_OUTWARD)) )== NULL )
		return NULL;

	fws_global_visibility->fws_justify = FWS_WATCH_LIKE_BLOCKED;
	return (const FWS_DS_OUTWARD *) fws_global_visibility;
}
//#FWS_DS_OUTWARD *fws_return_visibility_blocked
