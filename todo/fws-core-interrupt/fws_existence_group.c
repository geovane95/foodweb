/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "fws_existence_group.h"

//#const FWS_DS_GROUP *fws_grow_new_existence_group
const FWS_DS_GROUP *fws_grow_new_existence_group(void)
{
	FWS_DS_GROUP *fws_new_avail_existences = NULL;
	const unsigned short fws_group_count = fws_amount_units(FWS_MAIN_WAY_EXISTENCE);
	const char **fws_found_units = fws_promote_group_units(FWS_MAIN_WAY_EXISTENCE);

	if(fws_found_units == NULL || fws_group_count < 1)
		goto fws_final_valgroup;

	if( ( fws_new_avail_existences = (FWS_DS_GROUP *) malloc(sizeof(FWS_DS_GROUP)) ) ==NULL )
		goto fws_final_valgroup;

	if( ( fws_new_avail_existences->fws_all_sfiles = (FWS_DS_SRC_FILE *) calloc(fws_group_count, sizeof(FWS_DS_SRC_FILE)) ) ==NULL )
		return NULL;

	for(unsigned short fws_iter_new_srs = 0; fws_iter_new_srs < fws_group_count; fws_iter_new_srs++)
	{
		FWS_DS_SRC_FILE *fws_iter_file_for_src = fws_open_it(*(fws_found_units+fws_iter_new_srs));
		memcpy(fws_new_avail_existences->fws_all_sfiles+fws_iter_new_srs, fws_iter_file_for_src, sizeof(fws_iter_file_for_src));
	}	


	if( (fws_new_avail_existences->fws_main_way = (char *) calloc(strlen(FWS_MAIN_WAY_EXISTENCE), sizeof(char)) ) ==NULL )
		return NULL;


	strcpy(fws_new_avail_existences->fws_main_way, FWS_MAIN_WAY_EXISTENCE);


	fws_final_valgroup:
	return (const FWS_DS_GROUP *) fws_new_avail_existences;
}
//#const FWS_DS_GROUP *fws_grow_new_existence_group
