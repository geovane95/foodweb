/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "fws_group.c"


//#const unsigned short fws_amount_units
const unsigned short fws_amount_units(char *fws_basic_dir)
{
	unsigned short fws_the_amount = 0;
	DIR *fws_obj_dir = NULL;
	FWS_DS_DIRN *fws_procs = NULL;

	if(fws_basic_dir == NULL || strlen(fws_basic_dir) < sizeof(char))
		return fws_the_amount;

	if( (fws_obj_dir = opendir(fws_basic_dir)) ==NULL )
		return fws_the_amount;

	 while (fws_procs = readdir(fws_obj_dir))
		if(strcmp(fws_procs->d_name, FWS_AVOID_SINGLE_UNIT) != 0 || strcmp(fws_procs->d_name, FWS_AVOID_DOUBLE_UNIT) != 0)
			fws_the_amount+=1;


	return fws_the_amount; 
}
//#const unsigned short fws_amount_units

//#const char **fws_promote_group_units
const char **fws_promote_group_units(char *fws_basic_dir)
{
	char **fws_units = NULL;
	DIR *fws_obj_dir = NULL;
	FWS_DS_DIRN *fws_procs = NULL;
	unsigned short fws_the_amount = fws_amount_units(fws_basic_dir);

	if(fws_basic_dir == NULL || strlen(fws_basic_dir) < sizeof(char))
		goto fws_found_units;

	if( (fws_obj_dir = opendir(fws_basic_dir)) ==NULL )
		goto fws_found_units;
	
	if( fws_the_amount < 1 && (fws_units = (char **) calloc(fws_the_amount, sizeof(char *))) ==NULL )
		goto fws_found_units;

	fws_the_amount-=1;
	while (fws_procs = readdir(fws_obj_dir))
	{
		if(strcmp(fws_procs->d_name, FWS_AVOID_SINGLE_UNIT) == 0 || strcmp(fws_procs->d_name, FWS_AVOID_DOUBLE_UNIT) == 0)
			continue;


		if( (*(fws_units+fws_the_amount) = (char *) calloc(strlen(fws_procs->d_name), sizeof(char))) ==NULL )
		{
			return NULL;
		}
		else
		{
			memset(*(fws_units+fws_the_amount), 0, strlen(fws_procs->d_name));
			strcpy(*(fws_units+fws_the_amount), fws_procs->d_name);
		}
		fws_the_amount-=1;
	}


	fws_found_units:
	return (const char **) fws_units;
}
//#const char **fws_promote_group_units
