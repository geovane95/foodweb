/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "fws_io.h"

//#char *fws_read_just_line_at
char *fws_read_just_line_at(unsigned short fws_index, const FILE *fws_src_file)
{
	unsigned short fws_iter_for_chars = 0;
	char fws_char_by_char = '\0';
	char *fws_line = NULL;

	if(fws_index < 1 || fws_src_file == NULL)
		goto fws_final_line;

	if(fws_index == 1)
	{
		if( (fws_line = (char *) calloc(FWS_MAX_CHARS_PER_LINE, sizeof(char))) ==NULL )
			return NULL;

		while( (fws_char_by_char = getc(fws_src_file)) != EOF )
		{
			if(fws_char_by_char != '\n')
			{
				*(fws_line+fws_iter_for_chars) = fws_char_by_char;
			}
			else
			{
				break;
			}
			fws_iter_for_chars+=1;
		}
	}
	else
	{
		if( (fws_line = (char *) calloc(FWS_MAX_CHARS_PER_LINE, sizeof(char))) ==NULL )
			return NULL;

		while( (fws_char_by_char = getc(fws_src_file)) != EOF )
		{
			if(fws_char_by_char != '\n' && fws_index != 1)
			{
				continue;
			}
			else if(fws_char_by_char != '\n' && fws_index == 1)
			{
				*(fws_line+fws_iter_for_chars) = fws_char_by_char;
				fws_iter_for_chars+=1;
			}
			else
			{
				fws_index-=1;
			}
		}

	}

	fws_final_line:
	return fws_line;
}
//#char *fws_read_just_line_at

//#const FWS_DS_SRC_FILE *fws_open_it
const FWS_DS_SRC_FILE *fws_open_it(char *fws_path_to_src)
{
	FILE *fws_raw_file = NULL;
	FWS_DS_SRC_FILE *fws_build_new_src = NULL;

	if(fws_path_to_src == NULL || strlen(fws_path_to_src) <= sizeof(char) || fws_get_src_extension(fws_path_to_src) == 1)
		return NULL;

	if( (fws_raw_file = fopen(fws_path_to_src,"r")) )
	{
		if( (fws_build_new_src = (FWS_DS_SRC_FILE *) malloc(sizeof(FWS_DS_SRC_FILE))) ==NULL )
			goto fws_end;

		if ( (fws_build_new_src->fws_file_name = (char *) calloc(strlen(fws_path_to_src),sizeof(char))) ==NULL )
			return NULL;

		if( (fws_build_new_src->fws_num_lines = fws_get_src_raw_size(fws_raw_file)) ==0 )
			return NULL;

		if( (fws_build_new_src->fws_file_size = fws_get_src_raw_size(fws_raw_file)) <1 )
			return NULL;

		if( (fws_build_new_src->fws_raw_lines = (char **) calloc(fws_build_new_src->fws_num_lines, sizeof(char *))) ==NULL )
			return NULL;


		for(unsigned int fws_lloc_line = (fws_build_new_src->fws_num_lines - 1);  fws_lloc_line > 0; fws_lloc_line--)
		{
			if ( (*(fws_build_new_src->fws_raw_lines+fws_lloc_line) = (char *) calloc(FWS_MAX_CHARS_PER_LINE,sizeof(char))) ==NULL)
				return NULL;


			strcpy(*(fws_build_new_src->fws_raw_lines+fws_lloc_line), fws_read_just_line_at(fws_lloc_line+1));
		}
		strcpy(fws_build_new_src->fws_file_name, fws_path_to_src);
	}


	fws_end_src:
	return (const FWS_DS_SRC_FILE *) fws_build_new_src;
}
//#const FWS_DS_SRC_FILE *fws_open_it
