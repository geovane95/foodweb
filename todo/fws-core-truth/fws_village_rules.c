/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "fws_village_rules.h"

//#void fws_specify_village_rules
void fws_specify_village_rules(FWS_DS_VILLAGE_RULES *fws_our_V_rules)
{
	if(fws_our_V_rules == NULL || (sizeof(fws_our_V_rules)) <= (sizeof(char)))
		return;

	fws_our_V_rules->fws_valid_top_token = FWS_TOP_VILLAGE_TOKEN;
	fws_our_V_rules->fws_valid_bottom_token = FWS_BOTTOM_VILLAGE_TOKEN;
	fws_our_V_rules->fws_valid_border_token =FWS_BORDER_VILLAGE_TOKEN;
}
//#void fws_specify_village_rules

//#unsigned char fws_integrity_by_village
unsigned char fws_integrity_by_village(const char *fws_string_job, FWS_DS_VILLAGE_RULES *fws_our_V_rules)
{
	return 1;
}
//#unsigned char fws_integrity_by_village
