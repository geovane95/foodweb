/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "fws_checkout.h"

//#unsigned char fws_get_src_extension
unsigned char fws_get_src_extension(const char *fws_file_unit_name)
{
	unsigned short fws_pointing = 0;
	unsigned short fws_len_of_extension = (unsigned short) strlen(FWS_BASIC_EXTENSION_FOR_SRC);
	unsigned short fws_len_unit_name = (unsigned short) strlen(fws_file_unit_name);

	if(fws_file_unit_name == NULL || fws_len_unit_name <= fws_len_of_extension)
		return 1;

	fws_pointing =	fws_len_unit_name - fws_len_of_extension;
	for(unsigned short fws_it_for_extension = 0; fws_it_for_extension < fws_len_of_extension; fws_it_for_extension++, fws_pointing++)
	{
		if(*(fws_file_unit_name+fws_pointing) == *((char *) (FWS_BASIC_EXTENSION_FOR_SRC+fws_it_for_extension)))
		{
			continue;
		}
		else
		{
			return 1;
		}
	}
	return 0;
}
//#unsigned char fws_get_src_extension

//#unsigned short fws_get_src_raw_size
unsigned short fws_get_src_raw_size(const FILE *fws_file_unit)
{
	if(fws_file_unit == NULL)
		return 1;

	if ( (fseek(fws_file_unit, 0 , SEEK_END)) != 0 )
		return 1;

	return (ftell(fws_file_unit) > sizeof(int)) ? (unsigned short) ftell(fws_file_unit) : 1;
}
//#unsigned short fws_get_src_raw_size

//#unsigned int fws_get_src_num_of_vlines
unsigned int fws_get_src_num_of_vlines(const FILE *fws_file_unit)
{
	unsigned int fws_num_lines = 0;
	char fws_tmp_attr = '\0';

	if(fws_file_unit == NULL)
		return 1;

	while( (fws_tmp_attr = getc(fws_file_unit)) != EOF )
	{
		if(fws_tmp_attr == '\n')
			fws_num_lines+=1;
	}
	return fws_num_lines;
}
//#unsigned int fws_get_src_num_of_vlines

//#const char **fws_get_words
const char **fws_get_words(char *fws_code_block)
{
	char **fws_processing_words = NULL;
	unsigned short fws_word_blocks_num = 0;
	unsigned short fws_code_block_len = strlen(fws_code_block);

	if(fws_code_block == NULL || fws_code_block_len < sizeof(char))
		return NULL;

	for(unsigned short fws_iter_on_blocks = 0; fws_iter_on_blocks < fws_code_block_len; fws_iter_on_blocks++)
		if(*(fws_code_block+fws_iter_on_blocks) == ' ')
			fws_word_blocks_num+=1;


	if( ( fws_processing_words = (char **) calloc(fws_word_blocks_num, sizeof(char *)) ) ==NULL )
	{
		return NULL;
	}
	else
	{
		for(unsigned short fws_iter_for_lloc = 0; fws_iter_for_lloc < fws_word_blocks_num; fws_iter_for_lloc++)
			if( ( *(fws_processing_words+fws_iter_for_lloc) = (char *) calloc(FWS_MAX_SINGLE_WORD_LEN, sizeof(char)) ) ==NULL )
				return NULL;


		for(unsigned short fws_iter_for_cpy = 0, unsigned short fws_iter_for_unit = 0; fws_iter_for_cpy < fws_code_block_len; fws_iter_for_cpy++)
		{
		}
	}


	return (const char **) fws_processing_words;
}
//#const char **fws_get_words
