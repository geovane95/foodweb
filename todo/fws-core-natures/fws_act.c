/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "fws_act.h"

//#const FWS_DS_ACT *fws_behavior_act_like_void
const FWS_DS_ACT *fws_behavior_act_like_void(void)
{
	FWS_DS_ACT *fws_it_acts_for = NULL;
	if( (fws_it_acts_for = (FWS_DS_ACT *) malloc(sizeof(FWS_DS_ACT)))== NULL )
		return NULL;

	fws_it_acts_for->fws_return_a = NULL;
	return (const FWS_DS_ACT *) fws_it_acts_for;
}
//#const FWS_DS_ACT *fws_behavior_act_like_void

//#const FWS_DS_ACT *fws_behavior_act_like_decimal
const FWS_DS_ACT *fws_behavior_act_like_decimal(FWS_DS_DATA_DECIMAL fws_returns_decimal_val)
{
	FWS_DS_ACT *fws_it_acts_for = NULL;
	if( (fws_it_acts_for = (FWS_DS_ACT *) malloc(sizeof(FWS_DS_ACT)))== NULL || fws_returns_decimal_val == NULL)
		return NULL;

	fws_it_acts_for->fws_return_a = &fws_returns_decimal_val.fws_value;
	return (const FWS_DS_ACT *) fws_it_acts_for;
}
//#const FWS_DS_ACT *fws_behavior_act_like_decimal

//#const FWS_DS_ACT *fws_behavior_act_like_bool
const FWS_DS_ACT *fws_behavior_act_like_bool(FWS_DS_DATA_BOOL fws_returns_bool_val)
{
	FWS_DS_ACT *fws_it_acts_for = NULL;
	if( (fws_it_acts_for = (FWS_DS_ACT *) malloc(sizeof(FWS_DS_ACT)))== NULL || fws_returns_bool_val == NULL)
		return NULL;

	fws_it_acts_for->fws_return_a = &fws_returns_bool_val.fws_value;
	return (const FWS_DS_ACT *) fws_it_acts_for;
}
//#const FWS_DS_ACT *fws_behavior_act_like_bool

//#const FWS_DS_ACT *fws_behavior_act_like_existence
const FWS_DS_ACT *fws_behavior_act_like_existence(FWS_DS_DATA_EXISTENCE fws_returns_existence_val)
{
	FWS_DS_ACT *fws_it_acts_for = NULL;
	if( (fws_it_acts_for = (FWS_DS_ACT *) malloc(sizeof(FWS_DS_ACT)))== NULL || fws_returns_existence_val == NULL)
		return NULL;

	fws_it_acts_for->fws_return_a = fws_returns_existence_val.fws_to_existence;
	return (const FWS_DS_ACT *) fws_it_acts_for;
}
//#const FWS_DS_ACT *fws_behavior_act_like_existence
