/*
Copyright 2017, Matheus H. Silva.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "fws_behavior_rules.h"

//#void fws_especify_behavior_rules
void fws_especify_behavior_rules(FWS_DS_BEHAVIOR_RULES *fws_our_B_rules)
{
	if(fws_our_B_rules == NULL || (sizeof(fws_our_B_rules)) < (sizeof(char)))
		return;

	if( ( fws_our_B_rules->fws_pre_key = (FWS_DS_BEHAVIOR_RULES *) calloc(strlen(FWS_PRE_BEHAVIOR_KEY), sizeof(char)) )== NULL )
		return;

	if( ( fws_our_B_rules->fws_pos_key = (FWS_DS_BEHAVIOR_RULES *) calloc(strlen(FWS_POS_BEHAVIOR_KEY), sizeof(char)) )== NULL )
		return;

	strcat(fws_our_B_rules->fws_pre_key, (const char *) FWS_PRE_BEHAVIOR_KEY);
	strcat(fws_our_B_rules->fws_pos_key, (const char *) FWS_POS_BEHAVIOR_KEY);
}
//#void fws_especify_behavior_rules

//#unsigned char fws_verify_integrity_for_behavior
unsigned char fws_verify_integrity_for_behavior(const char *fws_string_job, FWS_DS_BEHAVIOR_RULES *fws_our_B_rules)
{
	return 1;
}
//#unsigned char fws_verify_integrity_for_behavior
